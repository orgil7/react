import React, {useState} from 'react';

const Counter = () => {
    // var count = 0;
    let [count, setCount] = useState(0);

    const minus = () => {
        setCount(count--);
    }
    const add = () => {
        setCount(count++);
    }
    return (
        <>
        <button onClick={minus}>-</button>
        <input value={count} />
        <button onClick={add}>+</button>
        </>
    );
}
export default Counter;