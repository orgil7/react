import React from 'react';
import data from './data.json'

function Map (){
    return(
    <>
    <table>
    {data.map((item) =>{
        return(
            <>
            <tr >
                <td  className="data">{item.ID}</td>
                <td className="data">{item.Name}</td>
                <td  className="data">{item.Gender}</td>
                <td  className="data">{item.Class}</td>
                <td  className="data">{item.Seat}</td>
                <td  className="data">{item.Club}</td>
                <td  className="data">{item.Persona}</td>
                <td  className="data">{item.Crush}</td>
                <td  className="data">{item.BreastSize}</td>
                <td  className="data">{item.Strength}</td>
                <td  className="data">{item.Hairstyle}</td>
                <td  className="data">{item.Color}</td>
                <td className="data">{item.Eyes}</td>
                <td className="data">{item.EyeType}</td>
                <td className="data">{item.Stockings}</td>
                <td className="data">{item.Accessory}</td>
            </tr>
            </>
            )
    })}
    </table>
    </>
    )
}
export default Map;