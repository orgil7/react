import React, { useState } from 'react';
import './tabulate.css';
import data from './component/data.json';

// var studentData_ID = [];
const Tabulate = () =>{
    const [curr , setCurr] = useState(1);
    var total = data.length;
    var currentPage = curr;
    var limit = 20;
    var startIndex =  (currentPage - 1)*limit;
    var endIndex = currentPage * limit;
    var Datalist = [];
    var maxPage = total / limit;

    // const [dummyData, setDummyData] = useState();
    
    // useEffect(() => {
    //     setDummyData(data[0]);
    // }, [dummyData]);

    const Test = (sIndex, eIndex) => {   
        for(let i = sIndex; i < eIndex; i++){
            Datalist.push(data[i]);
        }
    }
    Test(startIndex, endIndex);
    const changePage_toStart = () => {
        // console.log("click", e);
        setCurr(1);
    }
    const changePage_toEnd = () => {
        setCurr(maxPage);
    }
    const changePage_toNext = () => {
        setCurr(curr + 1);
    }
    const changePage_toPre = () => {
        setCurr(curr - 1);
    }
    if(curr === 1 || curr === 2 || curr === maxPage - 1 || curr === maxPage){
        if(curr === 1){
            return(
                <>
                <table key={20016}>
                    <tr key={20017}>
                        <th key={20000} className="myStyle">ID</th>
                        <th key={20001} className="myStyle">Name</th>
                        <th key={20002} className="myStyle">Gender</th>
                        <th key={20003} className="myStyle">Class</th>
                        <th key={20004} className="myStyle">Seat</th>
                        <th key={20005} className="myStyle">Club</th>
                        <th key={20006} className="myStyle">Persona</th>
                        <th key={20007} className="myStyle">Crush</th>
                        <th key={20008} className="myStyle">BreastSize</th>
                        <th key={20009} className="myStyle">Strength</th>
                        <th key={20010} className="myStyle">Hairstyle</th>
                        <th key={20011} className="myStyle">Color</th>
                        <th key={20012} className="myStyle">Eyes</th>
                        <th key={20013} className="myStyle">EyeType</th>
                        <th key={20014} className="myStyle">Stockings</th>
                        <th key={20015} className="myStyle">Accessory</th>
                    </tr>
                        {Datalist.map((item, index) => {
                            return (
                        <>
                            <tr key = {index + 14400}>
                                <td key={index} className="data">{item.ID}</td>
                                <td key={index + 900} className="data">{item.Name}</td>
                                <td key={index + 1800} className="data">{item.Gender}</td>
                                <td key={index + 2700} className="data">{item.Class}</td>
                                <td key={index + 3600} className="data">{item.Seat}</td>
                                <td key={index + 4500} className="data">{item.Club}</td>
                                <td key={index + 5400} className="data">{item.Persona}</td>
                                <td key={index + 6300} className="data">{item.Crush}</td>
                                <td key={index + 7200} className="data">{item.BreastSize}</td>
                                <td key={index + 8100} className="data">{item.Strength}</td>
                                <td key={index + 9000} className="data">{item.Hairstyle}</td>
                                <td key={index + 9900} className="data">{item.Color}</td>
                                <td key={index + 10800} className="data">{item.Eyes}</td>
                                <td key={index + 11700} className="data">{item.EyeType}</td>
                                <td key={index + 12600} className="data">{item.Stockings}</td>
                                <td key={index + 13500} className="data">{item.Accessory}</td>
                            </tr>
                        </>
                            )
                        })}
                    </table>
                        <button onClick={changePage_toStart} style={{display: 'inline-block'}}>{1}</button>
                        <button onClick={changePage_toNext} style={{display: 'inline-block'}}>{2}</button>
                        <p style={{display: "inline-block"}}>...</p>
                        <button onClick={changePage_toEnd} style={{display: 'inline-block'}}>{maxPage}</button>
                        <button onClick={changePage_toNext} style={{display: "inline-block"}}>{`>`}</button>
                        <button onClick={changePage_toEnd} style={{display: "inline-block"}}>{`>>`}</button>
                </>
            );
        }
        else{
            if(curr === 2){
                    return(
                        <>
                        <table key={20016}>
                            <tr key={20017}>
                                <th key={20000} className="myStyle">ID</th>
                                <th key={20001} className="myStyle">Name</th>
                                <th key={20002} className="myStyle">Gender</th>
                                <th key={20003} className="myStyle">Class</th>
                                <th key={20004} className="myStyle">Seat</th>
                                <th key={20005} className="myStyle">Club</th>
                                <th key={20006} className="myStyle">Persona</th>
                                <th key={20007} className="myStyle">Crush</th>
                                <th key={20008} className="myStyle">BreastSize</th>
                                <th key={20009} className="myStyle">Strength</th>
                                <th key={20010} className="myStyle">Hairstyle</th>
                                <th key={20011} className="myStyle">Color</th>
                                <th key={20012} className="myStyle">Eyes</th>
                                <th key={20013} className="myStyle">EyeType</th>
                                <th key={20014} className="myStyle">Stockings</th>
                                <th key={20015} className="myStyle">Accessory</th>
                            </tr>
                                {Datalist.map((item, index) => {
                                    return (
                                <>
                                    <tr key = {index + 14400}>
                                        <td key={index} className="data">{item.ID}</td>
                                        <td key={index + 900} className="data">{item.Name}</td>
                                        <td key={index + 1800} className="data">{item.Gender}</td>
                                        <td key={index + 2700} className="data">{item.Class}</td>
                                        <td key={index + 3600} className="data">{item.Seat}</td>
                                        <td key={index + 4500} className="data">{item.Club}</td>
                                        <td key={index + 5400} className="data">{item.Persona}</td>
                                        <td key={index + 6300} className="data">{item.Crush}</td>
                                        <td key={index + 7200} className="data">{item.BreastSize}</td>
                                        <td key={index + 8100} className="data">{item.Strength}</td>
                                        <td key={index + 9000} className="data">{item.Hairstyle}</td>
                                        <td key={index + 9900} className="data">{item.Color}</td>
                                        <td key={index + 10800} className="data">{item.Eyes}</td>
                                        <td key={index + 11700} className="data">{item.EyeType}</td>
                                        <td key={index + 12600} className="data">{item.Stockings}</td>
                                        <td key={index + 13500} className="data">{item.Accessory}</td>
                                    </tr>
                                </>
                                    )
                                })}
                            </table>
                                <button onClick={changePage_toStart} style={{display: 'inline-block'}}>{1}</button>
                                <button style={{display: 'inline-block'}}>{2}</button>
                                <button onClick={changePage_toNext} style={{display: 'inline-block'}}>{3}</button>
                                <p style={{display: "inline-block"}}>...</p>
                                <button onClick={changePage_toEnd} style={{display: 'inline-block'}}>{maxPage}</button>
                                <button onClick={changePage_toNext} style={{display: "inline-block"}}>{`>`}</button>
                                <button onClick={changePage_toEnd} style={{display: "inline-block"}}>{`>>`}</button>
                        </>
                    );
            }
            else{
                if(curr === maxPage - 1){
                    return(
                        <>
                        <table key={20016}>
                            <tr key={20017}>
                                <th key={20000} className="myStyle">ID</th>
                                <th key={20001} className="myStyle">Name</th>
                                <th key={20002} className="myStyle">Gender</th>
                                <th key={20003} className="myStyle">Class</th>
                                <th key={20004} className="myStyle">Seat</th>
                                <th key={20005} className="myStyle">Club</th>
                                <th key={20006} className="myStyle">Persona</th>
                                <th key={20007} className="myStyle">Crush</th>
                                <th key={20008} className="myStyle">BreastSize</th>
                                <th key={20009} className="myStyle">Strength</th>
                                <th key={20010} className="myStyle">Hairstyle</th>
                                <th key={20011} className="myStyle">Color</th>
                                <th key={20012} className="myStyle">Eyes</th>
                                <th key={20013} className="myStyle">EyeType</th>
                                <th key={20014} className="myStyle">Stockings</th>
                                <th key={20015} className="myStyle">Accessory</th>
                            </tr>
                                {Datalist.map((item, index) => {
                                    return (
                                <>
                                    <tr key = {index + 14400}>
                                        <td key={index} className="data">{item.ID}</td>
                                        <td key={index + 900} className="data">{item.Name}</td>
                                        <td key={index + 1800} className="data">{item.Gender}</td>
                                        <td key={index + 2700} className="data">{item.Class}</td>
                                        <td key={index + 3600} className="data">{item.Seat}</td>
                                        <td key={index + 4500} className="data">{item.Club}</td>
                                        <td key={index + 5400} className="data">{item.Persona}</td>
                                        <td key={index + 6300} className="data">{item.Crush}</td>
                                        <td key={index + 7200} className="data">{item.BreastSize}</td>
                                        <td key={index + 8100} className="data">{item.Strength}</td>
                                        <td key={index + 9000} className="data">{item.Hairstyle}</td>
                                        <td key={index + 9900} className="data">{item.Color}</td>
                                        <td key={index + 10800} className="data">{item.Eyes}</td>
                                        <td key={index + 11700} className="data">{item.EyeType}</td>
                                        <td key={index + 12600} className="data">{item.Stockings}</td>
                                        <td key={index + 13500} className="data">{item.Accessory}</td>
                                    </tr>
                                </>
                                    )
                                })}
                            </table>
                            <button onClick={changePage_toStart} style={{display: 'inline-block'}}>{`<<`}</button>
                            <button onClick={changePage_toPre} style={{display: 'inline-block'}}>{`<`}</button>
                            <button onClick={changePage_toStart} style={{display: 'inline-block'}}>{1}</button>
                            <p style={{display: "inline-block"}}>...</p>
                            <button style={{display: 'inline-block'}}>{maxPage - 1}</button>
                            <button onClick={changePage_toEnd} style={{display: 'inline-block'}}>{maxPage}</button>
                        </>
                    );
                }
                else{
                    return(
                        <>
                        <table key={20016}>
                            <tr key={20017}>
                                <th key={20000} className="myStyle">ID</th>
                                <th key={20001} className="myStyle">Name</th>
                                <th key={20002} className="myStyle">Gender</th>
                                <th key={20003} className="myStyle">Class</th>
                                <th key={20004} className="myStyle">Seat</th>
                                <th key={20005} className="myStyle">Club</th>
                                <th key={20006} className="myStyle">Persona</th>
                                <th key={20007} className="myStyle">Crush</th>
                                <th key={20008} className="myStyle">BreastSize</th>
                                <th key={20009} className="myStyle">Strength</th>
                                <th key={20010} className="myStyle">Hairstyle</th>
                                <th key={20011} className="myStyle">Color</th>
                                <th key={20012} className="myStyle">Eyes</th>
                                <th key={20013} className="myStyle">EyeType</th>
                                <th key={20014} className="myStyle">Stockings</th>
                                <th key={20015} className="myStyle">Accessory</th>
                            </tr>
                                {Datalist.map((item, index) => {
                                    return (
                                <>
                                    <tr key = {index + 14400}>
                                        <td key={index} className="data">{item.ID}</td>
                                        <td key={index + 900} className="data">{item.Name}</td>
                                        <td key={index + 1800} className="data">{item.Gender}</td>
                                        <td key={index + 2700} className="data">{item.Class}</td>
                                        <td key={index + 3600} className="data">{item.Seat}</td>
                                        <td key={index + 4500} className="data">{item.Club}</td>
                                        <td key={index + 5400} className="data">{item.Persona}</td>
                                        <td key={index + 6300} className="data">{item.Crush}</td>
                                        <td key={index + 7200} className="data">{item.BreastSize}</td>
                                        <td key={index + 8100} className="data">{item.Strength}</td>
                                        <td key={index + 9000} className="data">{item.Hairstyle}</td>
                                        <td key={index + 9900} className="data">{item.Color}</td>
                                        <td key={index + 10800} className="data">{item.Eyes}</td>
                                        <td key={index + 11700} className="data">{item.EyeType}</td>
                                        <td key={index + 12600} className="data">{item.Stockings}</td>
                                        <td key={index + 13500} className="data">{item.Accessory}</td>
                                    </tr>
                                </>
                                    )
                                })}
                            </table>
                            <button onClick={changePage_toStart} style={{display: 'inline-block'}}>{`<<`}</button>
                            <button onClick={changePage_toPre} style={{display: 'inline-block'}}>{`<`}</button>
                            <button onClick={changePage_toStart} style={{display: 'inline-block'}}>{1}</button>
                            <p style={{display: "inline-block"}}>...</p>
                            <button onClick={changePage_toPre} style={{display: 'inline-block'}}>{maxPage - 1}</button>
                            <button onClick={changePage_toEnd} style={{display: 'inline-block'}}>{maxPage}</button>
                        </>
                    );
                }
            }
        }
    }
    else{

        return(
            <>
            <table key={20016}>
                <tr key={20017}>
                    <th key={20000} className="myStyle">ID</th>
                    <th key={20001} className="myStyle">Name</th>
                    <th key={20002} className="myStyle">Gender</th>
                    <th key={20003} className="myStyle">Class</th>
                    <th key={20004} className="myStyle">Seat</th>
                    <th key={20005} className="myStyle">Club</th>
                    <th key={20006} className="myStyle">Persona</th>
                    <th key={20007} className="myStyle">Crush</th>
                    <th key={20008} className="myStyle">BreastSize</th>
                    <th key={20009} className="myStyle">Strength</th>
                    <th key={20010} className="myStyle">Hairstyle</th>
                    <th key={20011} className="myStyle">Color</th>
                    <th key={20012} className="myStyle">Eyes</th>
                    <th key={20013} className="myStyle">EyeType</th>
                    <th key={20014} className="myStyle">Stockings</th>
                    <th key={20015} className="myStyle">Accessory</th>
                </tr>
                    {Datalist.map((item, index) => {
                        return (
                    <>
                        <tr key = {index + 14400}>
                            <td key={index} className="data">{item.ID}</td>
                            <td key={index + 900} className="data">{item.Name}</td>
                            <td key={index + 1800} className="data">{item.Gender}</td>
                            <td key={index + 2700} className="data">{item.Class}</td>
                            <td key={index + 3600} className="data">{item.Seat}</td>
                            <td key={index + 4500} className="data">{item.Club}</td>
                            <td key={index + 5400} className="data">{item.Persona}</td>
                            <td key={index + 6300} className="data">{item.Crush}</td>
                            <td key={index + 7200} className="data">{item.BreastSize}</td>
                            <td key={index + 8100} className="data">{item.Strength}</td>
                            <td key={index + 9000} className="data">{item.Hairstyle}</td>
                            <td key={index + 9900} className="data">{item.Color}</td>
                            <td key={index + 10800} className="data">{item.Eyes}</td>
                            <td key={index + 11700} className="data">{item.EyeType}</td>
                            <td key={index + 12600} className="data">{item.Stockings}</td>
                            <td key={index + 13500} className="data">{item.Accessory}</td>
                        </tr>
                    </>
                        )
                    })}
                </table>
                    <button onClick={changePage_toStart} style={{display: 'inline-block'}}>{`<<`}</button>
                    <button onClick={changePage_toPre} style={{display: 'inline-block'}}>{`<`}</button>
                    <button onClick={changePage_toStart} style={{display: 'inline-block'}}>{1}</button>
                    <p style={{display: "inline-block"}}>...</p>
                    <button onClick={changePage_toPre} style={{display: "inline-block"}}>{currentPage - 1}</button>
                    <button style={{display: "inline-block"}}>{currentPage}</button>
                    <button onClick={changePage_toNext} style={{display: "inline-block"}}>{currentPage + 1}</button>
                    <p style={{display: "inline-block"}}>...</p>
                    <button onClick={changePage_toEnd} style={{display: 'inline-block'}}>{maxPage}</button>
                    <button onClick={changePage_toNext} style={{display: "inline-block"}}>{`>`}</button>
                    <button onClick={changePage_toEnd} style={{display: "inline-block"}}>{`>>`}</button>
            </>
        );
    }
}
export default Tabulate;